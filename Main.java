import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Figure figure = new Triangle(5.0, 7.0,10.0);
        Figure figure2 = new Circle(4.0);
        Figure figure3 = new Square(8.0);

        ArrayList<Figure> arrayList = new ArrayList<>();
        arrayList.add(figure);
        arrayList.add(figure2);
        arrayList.add(figure3);

        for (int i = 0; i < arrayList.size(); i++){
            System.out.println(arrayList.get(i).area());
        }
    }
}