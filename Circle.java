public class Circle extends Figure{
    private Double r;

    public Circle(Double r) {
        this.r = r;
    }

    @Override
    public Double area() {
        return 3.14*r*r;
    }
}
