public abstract class Figure {
    public abstract Double area();
}
