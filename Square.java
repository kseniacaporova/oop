public class Square extends Figure{
    private Double a;

    public Square(Double a) {
        this.a = a;
    }

    @Override
    public Double area() {
        return a * a;
    }
}
